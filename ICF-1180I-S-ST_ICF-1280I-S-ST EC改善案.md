# ICF-1180I-S-ST_ICF-1280I-S-ST EC改善案

[TOC]

## ICF-1180I-S-ST

### T0

1. CPLD(complex programmable logic device)燒錄

   ![](markdown_images/ICF-1180I_T0_CPLD.jpg)

2. MCU燒錄

   燒錄檔為**TIM_ADC_Trigger.hex** (待確認哪來的)

   ![](markdown_images/ICF-1180I_T0_MCU.jpg)



### T1

![](markdown_images/ICF-1180I_T1_MP.jpg)

1. M-00187: MGate 5101 用來做Modbus TCP轉PROFIBUS下command

2. 推測: PC傳一個TCP package -> MGate轉為PROFIBUS package -> DUT1 Fiber Tx forward package 至DUT2 Fiber Rx ->  DUT2 PROFIBUS forward package ->  [MG4101 modbus <->PROFIBUS](https://www.moxa.com/tw/products/industrial-edge-connectivity/protocol-gateways/fieldbus-gateways/mgate-4101-mb-pbs-series)-MB-PBS to ICF-1180x T1_T2_T3 PROFIBUS收封包 -> MG4101-MB-PBS to ICF-1180x T1_T2_T3 Modus loopback 再由PROFIBUS forward package

   ![](markdown_images/ICF-1180I_deepswitch.jpg)

3. LED燈測試

   ![](markdown_images/ICF-1180I_ledtest.jpg)

4. 軟體設定，T1測試完畢後DUT會被貼上QC OK貼紙

   ![](markdown_images/ICF-1180I_T0_setting.jpg)

   

### T2 Burn-in & T3 Fiber 衰減測試

![](markdown_images/ICF-1180I_T2.jpg)

![](markdown_images/ICF-1180I_T2-2.jpg)



## ICF-1280I-S-ST

### T1

1. T1 test with redundant ring function

![](markdown_images/ICF-1280I_T1_MP.jpg)

![](markdown_images/ICF-1280I_deepswitch.jpg)

2. LED test

3. 軟體設定，T1測試完畢後DUT會被貼上QC OK貼紙

   ![](markdown_images/ICF-1280I_T1_MP.jpg)

### T2 Burn-in & T3 Fiber衰檢測試

推測是N-00130 Fiber Rx收到後，加上Address資訊後由Fiber Tx傳送出去

![](markdown_images/ICF-1280I_T2-1.jpg)

![](markdown_images/ICF-1280I_T2-2.jpg)



## 改善案會議

[Profibus, Process Field Bus](https://zh.wikipedia.org/zh-tw/Profibus)轉Fiber

- Profibus是一種通訊協定，與Profinet是兩種不同的通訊協定
- 接頭為DP9



**改善案目的**

1. Sigle mode fiber供應商(sourcephotonics 索爾斯)EOL 2024/Q1
2. Fiber 選用Pin to Pin 的料件，僅置換Fiber 其他元件不進行變更

4. PM要確認各單位需要的人力資源



**HW**

- Fiber component Tx, Rx 頭會變成分開的，而過去是合在一起

- HW正在驗證新的料，但目前供應商還不是Moxa承認廠商

- 新料與舊料的操作溫度範圍相同 (-40~85度)

- 新料、舊料規格沒有太大差異 (pin腳和操作溫度相同)

  - Receiver

    ![receiver pin](markdown_images/ICF-1180I_ICF-1280I_Fiber_receiver_pin.jpg)

    

    ![receiver temperature](markdown_images/ICF-1180I_ICF-1280I_Fiber_receiver_temperature.jpg)

    

  - Transmitter

    ![transmitter pin](markdown_images/ICF-1180I_ICF-1280I_Fiber_transmitter_pin.jpg)

    

    ![transmitter temperature](markdown_images/ICF-1180I_ICF-1280I_Fiber_transmitter_temperature.jpg)



**SW**

- Protocol 目前PM認知不會更改



:exclamation: **Action item** 

- 3/9 (四)前要給PM各單位負責人

- BIOS目前評估不需要人力投入
  - 國偉: converter裡面沒有FW
  - 威舜: 產測治具Rx, Tx的頭原本就是分開的
  - 其他注意事項? (若產測過程可能會需要協助，code可以先study)